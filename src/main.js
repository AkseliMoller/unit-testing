// src/main.js

import mylib from './mylib';

const express = require('express')
const app = express()
const port = 3000
const mylib = require('./mylib');

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(mylib.sum(a,b));
    });

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
